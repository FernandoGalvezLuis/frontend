function DisplayGET() {
    fetch('http://localhost:5000/data',{
        method: 'GET',
        headers: 'Content-Type: application/json'
    })
    .then(res=>{
        if (res.ok){console.log("Success")}
        else{console.log("Not successful")}
        res.json()    })
    .then(data=> console.log(data))
    .catch(error => {console.log(`error`)})

console.log(fetch)

    return (
      <>
        
        Some text
      </>
    );
  }
  
  export default DisplayGET;

  /**
   * If we simply try to access our information like the code below we get 
   * an error stating that CORs policy doesn´t allow data coming from other URL
   * (in our case different localhost:number), because by default
   * it only accepts data coming from the same origin (same URL or localhost:number)
   * 
   * so we need to configure CORS to allow data from other URLs
   * 
   * 
   *  fetch('http://localhost:5000/data',{
        method: 'GET',
        headers: 'Content-Type: application/json'
    })
    .then(res=>{
        if (res.ok){console.log("Success")}
        else{console.log("Not successful")}
        res.json()    })
    .then(data=> console.log(data))
    .catch(error => {console.log(`error`)})
   */